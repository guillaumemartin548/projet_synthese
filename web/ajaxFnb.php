<?php
	require_once("action/ajax/AjaxFnbAction.php");

	$action = new AjaxFnbAction();
	$action->execute();

	echo json_encode($action->result);