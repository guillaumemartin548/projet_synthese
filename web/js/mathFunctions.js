const yearlyMarketOpenDays=250 //environ le nombre de jours que le TSX est ouvert 5 jrs/semaine -10 jours fériés

const add = (a, b) => a + b;
const multiply=(a,b)=>a*b;

function sumArray(array){
    return array.reduce(add);
}

function meanBuyPrice(fnb, nbTotal){
    let totalPriceperBuy=[]

    for (let i = 0; i < fnb.prix.length; i++) {
        totalPriceperBuy.push(fnb.nbTitres[i]*fnb.prix[i])
    }
    
    let totalPrice=totalPriceperBuy.reduce(add);

    return totalPrice/nbTotal
}

function averageReturn(meanBuyPrice,nbTitres, totalValue){
    return totalValue/(meanBuyPrice*nbTitres)*100-100
}

function averageAnnualReturn(values){

    let annualValues=[];
    let nbYears=-1;
    let result=0;

    for (let i = 0; i < values.length; i+=yearlyMarketOpenDays) {
        annualValues.push(values[i])
        nbYears+=1
    }

    let eachYearReturn=[]
    for (let i = 1; i <= nbYears; i++) {
        eachYearReturn.push(annualValues[i-1]/annualValues[i]*100-100) 
    }

    try {
        result=eachYearReturn.reduce(add)/nbYears
    } catch (error) {
        
    }
    return result
}

function totalReturn(){

}