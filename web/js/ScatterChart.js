//À COMPLÉTER

//let seed=Math.floor(Math.random())
//backgroundColor=randomColor({seed:seed,alpha: 0.50,hue: clustersHues[cluster], hue: clustersHues[cluster],format: 'rgba'});
//borderColor=randomColor({seed:seed,hue: clustersHues[cluster], hue: clustersHues[cluster],format: 'rgba'});

class ScatterChart extends Chart{
	constructor(ctx, data){
        super(ctx, {
            type: 'scatter',
            data: {
                labels: [], //symbole des FNB
                datasets: [{
                    label: 'Fnb Sélectionnés',
                    borderColor: randomColor({seed:1,hue: clustersHues[0], hue: clustersHues[0],format: 'rgba'}),
                    backgroundColor: randomColor({seed:1,alpha: 0.50,hue: clustersHues[0], hue: clustersHues[0],format: 'rgba'}),
                    data: [] //[{x: -10, y: 0}, {x: 0,y: 10}]
                }]

            },
            options: {
                scales: {
                    xAxes: [{
                        type: 'linear',
                        position: 'bottom'
                    }]
                }
                
            }
        });

    }
    
    //https://www.chartjs.org/docs/latest/developers/updates.html
    updateData(label, xyData,labels, datasetIndex, xLabel, yLabel) {  //xydata = [{x:11,y:11},{x:11,y:11}]
        this.data.datasets.label= xLabel+' / '+yLabel;
        this.data.labels.push(label);
        this.datasets.data=[];

        xyData.forEach((xyValue)=>{
            this.datasets.data.push(xyValue)
            this.data.labels.push
        })

        this.data.datasets.forEach((dataset) => {
            dataset.data.push(data);
        });
        this.update();
    }

    addDataset(label, xData,yData){  //data -> Tableau de valeur Y, les labels doivent être du même nombre

        let newDataset={
            x:xData,
            y:yData,
        }
        this.data.labels.push(label);
        this.data.datasets.push(newDataset);
        this.update();
    }

    removeDataset(label){
        let indexDataset = this.data.labels.indexOf(label);
        this.data.labels.splice(indexDataset, 1);
        this.data.datasets.splice(indexDataset, 1);
        this.update();
    }

}





