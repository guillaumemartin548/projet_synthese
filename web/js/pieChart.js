class PieChart extends Chart{
	constructor(ctx, data){
        super(ctx, {
            type: 'pie',
            data: {
                labels: [],
                datasets: [{
                    label: '# of Votes',
                    data: [],
                    backgroundColor: [],
                    borderColor: [],
                    borderWidth: 2
                }]
            },
            options: {
                // responsive: false,
            }
        });

    }

    //https://www.chartjs.org/docs/latest/developers/updates.html
    addData(nomFnb, valeurTotale, backgroundColor, borderColor) {
        this.data.labels.push(nomFnb);
        this.data.datasets.forEach((dataset) => {
            dataset.data.push(valeurTotale);
            dataset.backgroundColor.push(backgroundColor);
            dataset.borderColor.push(borderColor);
        });
        this.update();
    }
        
    removeData(nomFnb) {
        let indexDataset = this.data.labels.indexOf(nomFnb);

        this.data.labels.splice(indexDataset, 1);
        this.data.datasets.forEach((dataset) => {
            dataset.data.splice(indexDataset, 1);
        });
        this.update();
    }

    modifyData(nomFnb, valeurTotale){

        let indexDataset = this.data.labels.indexOf(nomFnb);

        this.data.datasets[0].data[indexDataset]=valeurTotale;

        this.update();
    }
    
}
        
