class RadarChart extends Chart{
	constructor(ctx, data){
        super(ctx, {
            type: 'radar',
            data: {
                labels: [['Rendement','annuel moyen'], 'Dividende', 'EPS', 'PE ratio', 'PB ratio'],
                datasets: []
            },
            options: {
                // responsive: false,
            }
        });

    }
    
    addDataset(fnbName, rendement,dividende,eps,peratio,pbratio, backgroundColor, borderColor){  //data -> Tableau de valeur Y, les labels doivent être du même nombre

        let newDataset={
            label: fnbName,
            backgroundColor: backgroundColor,
            borderColor: borderColor,
            pointBackgroundColor:backgroundColor,
            data: [rendement,dividende,eps,peratio,pbratio],
        }
        this.data.datasets.push(newDataset);
        this.update();
    }

    removeDataset(label){

        let indexDataset = null;
        for (let i = 0; i < this.data.datasets.length; i++) {
            if (this.data.datasets[i].label==label) {
                indexDataset=i;
                break;
            }
        }

        this.data.datasets.splice(indexDataset, 1);
        this.update();
    }


}
