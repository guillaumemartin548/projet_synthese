//voir à inclure le format date pour le X-axis
class LinearChart extends Chart{
	constructor(ctx, data){
        super(ctx, {
            type: 'line',
            data: {
                datasets: []
            },
            options: {
                // responsive: false,
                beginAtZero:false,
                scales: {
                    xAxes: [{
                        display: true,
                        scaleLabel: {
                            display: true,
                            labelString: 'Temps'
                        }
                    }],
                    yAxes: [{
                        display: true,
                        scaleLabel: {
                            display: true,
                            labelString: 'Rendement du titre'
                        }
                    }]
                },
                elements: {
                    point:{
                        radius: 1
                    }
                },
                title: {
                    display: true,
                    text: 'Rendement des titres'
                },
                tooltips: {
                    mode: 'index',
                    intersect: false,
                },
                hover: {
                    mode: 'nearest',
                    intersect: true
                },
            }
        });

        //initialisation des dates en utilisant le plus vieux FNB pour être certain d'avoir 10 ans de dates valides de transaction
        this.dates=getOneFNB('XIU').HistoriqueValeur.dates.reverse();
        this.nbDaysDisplayed=0
    }
    

    //https://www.chartjs.org/docs/latest/developers/updates.html
    //augmenter la durée de temps vérifiée
    addData(newLabels, values=[]) { // labels=dates,values=tableau de tableaux
        //values, array of array
        this.data.labels=newLabels;  //à vérifier pour le reverse
        let indexValues=0;
        this.data.datasets.forEach((dataset) => {
            dataset.data=values[indexValues];
            indexValues++;
        });
    }

    addDataset(label, backgroundColor, borderColor){  //data -> Tableau de valeur Y, les labels doivent être du même nombre

        let newDataset={
            label: label,
            backgroundColor: backgroundColor,
            borderColor: borderColor,
            fill: false
        }
        this.data.datasets.push(newDataset);
    }


    addLastDatasetData(){
        let data=selectionData[selectionData.length-1].HistoriqueValeur.valeurFermeture.slice(0,this.nbDaysDisplayed)

        if (data.length<this.nbDaysDisplayed){
            data=this.fillVoid(this.nbDaysDisplayed,data);
        }

        else{
            data=data.reverse();
        }

        this.data.datasets[this.data.datasets.length-1].data=data;

        this.update()
    }

    removeDataset(label){
        let indexDataset = null;
        for (let i = 0; i < this.data.datasets.length; i++) {
            if (this.data.datasets[i].label==label) {
                indexDataset=i;
                break;
            }
        }
        this.data.datasets.splice(indexDataset, 1);
        this.update();
    }


    changeDaysNumber(daysNumber){
        if (daysNumber!=this.nbDaysDisplayed) {
            this.nbDaysDisplayed=daysNumber;
            let datesData=this.dates.slice(-daysNumber)
            let values=[];
            selectionData.forEach(fnbData=>{
                let nbCloseValues=fnbData.HistoriqueValeur.valeurFermeture.length;
                if (nbCloseValues>=daysNumber) {
                    values.push(fnbData.HistoriqueValeur.valeurFermeture.slice(0,daysNumber).reverse());
                }
                else{
                    values.push(this.fillVoid(nbCloseValues,fnbData.HistoriqueValeur.valeurFermeture));
                }

            })
            this.addData(datesData, values)
        }

        this.update();
    }

//on doit ajouter des valeurs vides si le nombre de dates est insuffisants pour le fnb sinon la ligne part du début du graphique, ce qui est incorrect
    fillVoid(nbCloseValues, valeurFermeture){
        return Array(this.nbDaysDisplayed-nbCloseValues).concat(valeurFermeture.slice(0,nbCloseValues).reverse());

    }
}
