// https://github.com/davidmerfield/randomColor
const clustersHues=['red', 'orange', 'yellow', 'green', 'blue', 'purple', 'pink'];


let pieChart=null;
let linearChart=null;
let radarChart=null;
let scatterChart=null;

let pieChartNode=null;
let linearChartNode=null;
let radarChartNode=null;
let scatterChartNode=null;
let rendementTotalNode=null;
let valeurTotalNode=null;
let fondsDisponiblesNode=null;

let buttonPortefeuille=null;
let buttonLinear=null;
let buttonRadar=null;
let buttonScatter=null;

let activeChartNode=null;
let activeTabNode=null;

let tabPortefeuille=null;
let tabSelection=null;
let tabPortefeuilleBody=null;
let tabSelectionBody=null;

let scatterChartPanelNode=null;

let modalSigninNode=null;

let modalFnb=null;

let searchBarData=[];
let searchResultNode=null;

let listselection=[]
let listportefeuille=[]
let portefeuilleData=[]
let selectionData=[]

let fondsDispoApp=0;
let montantTotalApp=0

window.onload = () => {

    buttonPortefeuille=document.querySelector("#buttonPortefeuille");
    buttonLinear=document.querySelector("#buttonLinear");
    buttonRadar=document.querySelector("#buttonRadar");
    buttonScatter=document.querySelector("#buttonScatter");
    searchResultNode=document.querySelector("#searchResults");
    searchBarNode=document.querySelector("#searchBar");
    rendementTotalNode=document.querySelector("#rendementTotal");
    valeurTotalNode=document.querySelector("#valeurTotale");

    pieChartNode=document.querySelector("#PieChart");
    linearChartNode=document.querySelector("#LineChart");
    radarChartNode=document.querySelector("#RadarChart");
    scatterChartNode=document.querySelector("#ScatterChart");

    scatterChartPanelNode=document.querySelector("#scatterChartPanel");

    pieChart=new PieChart(pieChartNode,0);
    linearChart=new LinearChart(linearChartNode,0);
    radarChart=new RadarChart(radarChartNode,0);
    scatterChart=new ScatterChart(scatterChartNode,0);

    linearChartNode.style.display="none";
    radarChartNode.style.display="none";
    scatterChartPanelNode.style.display="none";
    activeChartNode=pieChartNode;

    tabPortefeuille=document.querySelector("#tabPortefeuille");
    tabSelection=document.querySelector("#tabSelection");

    tabPortefeuilleBody=document.querySelector("#tabPorteSelection");
    fondsDisponiblesNode=tabPortefeuilleBody.querySelector("#fondsDispoPort");
    tabSelectionBody=document.querySelector("#tabBodySelection");

    activeTabNode=tabPortefeuille;


    buttonPortefeuille.onclick=() => {showCanvas(pieChartNode);showTab(tabPortefeuille);};
    buttonLinear.onclick=() => {showCanvas(linearChartNode);showTab(tabSelection);};
    buttonRadar.onclick=() => {showCanvas(radarChartNode);showTab(tabSelection);};
    buttonScatter.onclick=() => {showCanvas(scatterChartPanelNode);showTab(tabSelection);};

    pieChartNode.onclick=(evt)=>{clickChart(pieChart, evt)}; //juste nécessaire pour les tests
    radarChartNode.onclick=(evt)=>{clickChart(radarChart, evt)};
    scatterChartNode.onclick=(evt)=>{clickChart(scatterChart, evt)};
    linearChartNode.onclick=(evt)=>{clickChart(linearChart, evt)};

    if (document.querySelector("#modalSignin")) {
        modalSigninNode=document.querySelector("#modalSignin");
        modalSigninButtonNode=modalSigninNode.querySelector("#buttonSignup");
        ConnectionButtonNode=modalSigninNode.querySelector("#buttonConnection");
    
        modalSigninButtonNode.onclick=()=>{addUser();}
        ConnectionButtonNode.onclick=()=>{getUser(true);}
    }

    else{
        getUser(false)
    }


    loadSearchBar();

    searchBarNode.onkeyup=function() {
        getSearchResults(searchBarNode.value);
    };
}

function initializeAppData(user){
    initializePortefeuille(user.portefeuille, user.fondsDisponibles, user.fondsInvestis);
    initializeSelection(user.selection);
    modifyTableButtons();
}

function initializePortefeuille(portefeuille,fondsDisponibles, fondsInvestis){
    let valeurPortefeuille=fondsDisponibles;
    fondsDispoApp=fondsDisponibles;
    montantTotalApp=fondsInvestis;

    fondsDisponiblesNode.value=fondsDisponibles;
    portefeuille.forEach((fnb) => {
        let fnbData=getOneFNB(fnb.symbole);
        portefeuilleData.push(fnbData);

        // let valeurTotale= fnb.
        let nbTitresTotal=sumArray(fnb.nbTitres);
        let coutMoyen=meanBuyPrice(fnb, nbTitresTotal)  //à ajouter au display
        let valeurTotale=nbTitresTotal*fnbData.close;
        let rendement=averageReturn(coutMoyen,nbTitresTotal,valeurTotale)
        valeurPortefeuille+=valeurTotale;
        addFnbDisplay(fnb.symbole, fnbData.titre,nbTitresTotal, coutMoyen, fnbData.close,valeurTotale,rendement,fnbData.cluster, 'portefeuille',true);
    })

    let rendementMoyen=(valeurPortefeuille/fondsInvestis*100-100).toFixed(2);
    rendementTotalNode.innerHTML=rendementMoyen+ ' %';
    valeurTotalNode.innerHTML=valeurPortefeuille.toFixed(2)+' $';
    montantTotalApp=valeurPortefeuille;

    fondsDisponiblesNode.onchange=function(){
        let valeur=fondsDisponiblesNode.value-fondsDispoApp;
        montantTotalApp+=valeur;
        valeurTotalNode.innerHTML=montantTotalApp.toFixed(2)+' $';
        updateCash(valeur);
        fondsDispoApp=fondsDisponiblesNode.value;
    }
}

function initializeSelection(selection){
    
    selection.forEach((fnb) => {
        let fnbData=getOneFNB(fnb);
        selectionData.push(fnbData)

        addFnbDisplay(fnb, fnbData.titre, 0,0,fnbData.close, 0,0,fnbData.cluster, 'selection',true);

        let seed=Math.floor(Math.random());
        let rendementAnnuelMoyen=averageAnnualReturn(fnbData.HistoriqueValeur.valeurFermeture)
        
        linearChart.addDataset(fnb,getColor(seed, 0.50,fnbData.cluster),getColor(seed, 1,fnbData.cluster));
        radarChart.addDataset(fnb,rendementAnnuelMoyen,fnbData.dividend, fnbData.eps,fnbData.PEratio,fnbData.PBratio,getColor(seed, 0.50,fnbData.cluster),getColor(seed, 1,fnbData.cluster));
    })

    linearChart.changeDaysNumber(20)

    // initializer les boutons de temps pour changer le linearchart
    let buttonsTimeNode=document.querySelector('#buttonsTime');
    buttonsTimeNode.querySelectorAll('button').forEach(button=>{
        button.onclick=()=>{
            if (listselection.length>0) { //on ne load pas les valeurs si la selection est vide
                linearChart.changeDaysNumber(button.value);
            }
        }
    });

}

function addUser(){

    let email=modalSigninNode.querySelector("#emailSignup").value;
    let password=modalSigninNode.querySelector("#passwordSignup").value;
    let prenom=modalSigninNode.querySelector("#firstNameSignup").value;
    let nom=modalSigninNode.querySelector("#lastNameSignup").value;
    let adresse=modalSigninNode.querySelector("#addressSignup").value;
    let dateNaissance=modalSigninNode.querySelector("#dateSignup").value;

    // reference: https://stackoverflow.com/questions/1423777/how-can-i-check-whether-a-radio-button-is-selected-with-javascript
    let genre=document.querySelectorAll('input[type="radio"]:checked').value;
    let fondsDisponibles=modalSigninNode.querySelector("#InitialAmountSignup").value;
    let intent="addUser";
    let typeClienSelect = modalSigninNode.querySelector("#investorTypesSignup");
    let typeClient = typeClienSelect.options[typeClienSelect.selectedIndex].text;

    $.ajax({
            url : "ajaxUser.php",
            type : "POST",
            data : {
                email:email,
                password:password,
                prenom: prenom,
                nom:nom,
                adresse: adresse,
                dateNaissance: dateNaissance,
                genre: genre,
                fondsDisponibles: fondsDisponibles,
                fondsInvestis: fondsDisponibles,
                typeClient: typeClient,
                intent:intent
            }
        })
        .done(function (msg) {
            let reponse = JSON.parse(msg);


        });
}

function getUser(fromModal){

    let email='alreadySignedIn';
    let password='alreadySignedIn';

    if (fromModal){
        email=modalSigninNode.querySelector("#emailConnection").value;
        password=modalSigninNode.querySelector("#passwordConnection").value;
    }

    let intent="getUser";

    $.ajax({
        async:false,
        url : "ajaxUser.php",
        type : "POST",
        data : {
            email:email,
            password:password,
            intent:intent
        }
    })
    .done(function (msg) {
        if (msg!=false) {
            let user = JSON.parse(msg);
            initializeAppData(user);
        }
        if (fromModal) {
            $('#modalSignin').modal('hide');
            document.body.removeChild(modalSigninNode);
        }
        
    });
   
}

function getOneFNB(fnbName){
    let reponse=null

    $.ajax({  //options: 1 fnb all infos, liste barre recherche: nom, desc, prix
        async:false,
        url : "ajaxFnb.php",
        type : "POST",
        data : {
            intent : "getOneFNB",
            symbole: fnbName
        }
    })
    .done(function (msg) {
        reponse = JSON.parse(msg);

    });

    return reponse;
}

function modifyUserFnb(symbole,nbTitres,prix,position,operation){

    let intent="modifyFnb";

    $.ajax({
        url : "ajaxUser.php",
        type : "POST",
        data : {
            symbole:symbole,    //string
            nbTitres:nbTitres,  //int
            prix:prix,          //int
            position:position,  //string (portefeuille/selection)
            intent:intent,      //string (modifyFnb)
            operation:operation //string (add,remove,update)
        }
    })
    .done(function (msg) {
    });

}

//eager loading pour éviter des call ajax à chaque key up dans la barre de recherche, requête pour retourner les infos essentielles seulement
function loadSearchBar(){
    $.ajax({ 
        url : "ajaxFnb.php",
        async:false,
        type : "POST",
        data : {
            intent : "getFNBSearchBar",
        }
    })
    .done(function (msg) {
        let reponse = JSON.parse(msg);

        reponse.forEach((fnb) => {
            searchBarData.push({
                bourse:fnb.bourse,
                symbole:fnb.symbole,
                titre:fnb.titre,
                close:fnb.close,
                cluster:fnb.cluster
            });
        })
    });
    
}


function updateCash(cashAmount){
    $.ajax({
        url : "ajaxUser.php",
        type : "POST",
        data : {
            fondsInvestis:cashAmount, 
            intent:'updateCash',     
        }
    })
    .done(function (msg) {
    });
}

function getSearchResults(searchString){ //fonction simple avec un RegEx, voir à améliorer pour sortir les symboles en premier

    const maxResults=5; //sortir juste les 5 premiers resultats
    let nbResults=0
    let arrayResult=[]
    let regEx=new RegExp(searchString, 'i');

    //reference: https://medium.com/front-end-weekly/3-things-you-didnt-know-about-the-foreach-loop-in-js-ff02cec465b1
    //impossible de break un for each donc if (nbResults<maxResults) est nécessaire pour au ne pas continuer de loader les resultats après en avoir trouvé 5
    searchBarData.forEach(elem=>{
        if (nbResults<maxResults) {
            if(regEx.test(elem.symbole)||
               regEx.test(elem.titre)){
                arrayResult.push(elem);
                nbResults++;
            }
        }
    })

    displaySearchResults(arrayResult);

}

function displaySearchResults(arrayResult){

    searchResultNode.innerHTML="";

    arrayResult.forEach(elem=>{
        let newOption=document.createElement("option");
        newOption.value=elem.symbole+" | "+elem.titre+ " | "+elem.close+"$";
        newOption.className=elem.symbole
        searchResultNode.appendChild(newOption);
    });

    searchBarNode.onchange =()=>{
        let symbole=searchBarNode.value.substr(0,searchBarNode.value.indexOf(' '))
        arrayResult.forEach(elem=>{
            if (elem.symbole==symbole) {
                CreateModalFnb(elem.symbole, elem.titre, elem.close, elem.cluster);
            }
        });
    };

}

//fonction pour créer une boite de choix pour un Fnb sélectionné
function CreateModalFnb(fnbName, titre, prix, cluster, search=true){
    
    //permet le reset de la barre de recherche si le modal est créée avec celle-ci
    if (search) {
        searchResultNode.innerHTML="";
        searchBarNode.blur();
        searchBarNode.value='';
        searchBarNode.onchange =()=>{};
    }

    //

    let template=document.querySelector("#templateSelectFnb").innerHTML;
    modalFnb = document.createElement("div");
    modalFnb.innerHTML=template;
    modalFnb.querySelector("#nomFnb").value=fnbName;

    let nbNode=modalFnb.querySelector("#nbTitres");

    let prixNode = modalFnb.querySelector("#coutAchat");
    prixNode.value=prix;

    modalFnb.className="modal show bg-light";
    modalFnb.id="modalFnb";
    document.body.appendChild(modalFnb);

    modalFnb.querySelector("#nbTitres").onchange=()=>{prixNode.value=nbNode.value*prix};
    modalFnb.querySelector("#ajoutPortefeuille").onclick=()=>{addFnbDisplay(fnbName,titre,nbNode.value, prix,prix,prixNode.value,0,cluster,'portefeuille',false);}; //je devrais get le fnb au lieu de juste passer le nom en params
    modalFnb.querySelector("#ajoutSelection").onclick=()=>{addFnbDisplay(fnbName,titre,nbNode.value, prix,prix,prixNode.value,0,cluster,'selection',false);};
}

//function qui initiliase tous les paramètres et éléments du DOM nécessaire pour le fonctionnement d'un ajout de fond dans l'application
function addFnbDisplay(fnbName, titre, nbTitres,coutMoyen, valeur,prixTotal, rendement,cluster, position, initializeApp){

    //le seed de la librairie random color permet d'aller recherche la meme couleur plus d'une fois
    let seed=Math.floor(Math.random())
    let backgroundColor=getColor(seed,0.5,cluster)
    let borderColor=getColor(seed,1,cluster)
    let lineNode=null;
    let fnbData=null;
    
    if (position=='portefeuille' && !listportefeuille.includes(fnbName)) {
        listportefeuille.push(fnbName);
        fnbData=getOneFNB(fnbName)
        portefeuilleData.push(fnbData);
        let template=document.querySelector("#tabLinePortefeuille").innerHTML;
        lineNode= document.createElement("tr");
        lineNode.innerHTML=template;
        let inputNbTitres=lineNode.querySelector(".nbTitres");
        lineNode.querySelector(".symbole").innerHTML=fnbName;
        lineNode.querySelector(".description").innerHTML=titre;
        inputNbTitres.value=nbTitres;
        lineNode.querySelector(".coutMoyen").innerHTML=coutMoyen.toFixed(2);
        lineNode.querySelector(".valeur").innerHTML=valeur.toFixed(2);
        lineNode.querySelector(".total").innerHTML=(parseFloat(prixTotal)).toFixed(2);
        lineNode.querySelector(".rendement").innerHTML=rendement.toFixed(2);
        // lineNode.querySelector(".documentation").innerHTML="";
        lineNode.className='linePortefeuille'
        tabPortefeuilleBody.prepend(lineNode);

        pieChart.addData(fnbName,prixTotal, backgroundColor, borderColor);

        if (!initializeApp) {//permet d'utiliser la même fonction pour initialiser le visuel de l'application au startup et d'ajouter des éléments par la suite
            modifyUserFnb(fnbName,nbTitres,valeur,'portefeuille','add');
        }
        

        if (listselection.includes(fnbName)) {
            lineNode.querySelector('.ajoutAutreTableau').style.display='none';
        }

        inputNbTitres.onchange=()=>{
            if (inputNbTitres.value=='' || inputNbTitres.value<=0) { //on traite ke changement de nb de titres seulement si la valeur est supérieure à 0 ou non nulle
                inputNbTitres.value=nbTitres;
            }
            else{
                updateFnbDisplay(fnbName, lineNode, nbTitres, inputNbTitres.value, valeur);
            }
        };

    }
    else if(!listselection.includes(fnbName)){
        listselection.push(fnbName);
        fnbData=getOneFNB(fnbName)

        let template=document.querySelector("#tabLineSelection").innerHTML;
        lineNode= document.createElement("tr");
        lineNode.innerHTML=template;
        lineNode.querySelector(".symbole").innerHTML=fnbName;
        lineNode.querySelector(".description").innerHTML=fnbData.titre;
        lineNode.querySelector(".valeur").innerHTML=fnbData.close;
        lineNode.className='lineSelection';
        // lineNode.querySelector(".documentation").innerHTML="";

        if (listportefeuille.includes(fnbName)) {
            lineNode.querySelector('.ajoutAutreTableau').style.display='none';
        }

        tabSelectionBody.prepend(lineNode);

        if (!initializeApp) {
            selectionData.push(fnbData);
            modifyUserFnb(fnbName,nbTitres,valeur,'selection','add');
            linearChart.addDataset(fnbName,backgroundColor,borderColor);
            linearChart.addLastDatasetData();
            let rendementAnnuelMoyen=averageAnnualReturn(fnbData.HistoriqueValeur.valeurFermeture)
            radarChart.addDataset(fnbName,rendementAnnuelMoyen,fnbData.dividend, fnbData.eps,fnbData.PEratio,fnbData.PBratio,getColor(seed, 0.50,fnbData.cluster),getColor(seed, 1,fnbData.cluster));
        }
        
    }
    if (lineNode) {
        lineNode.querySelector(".Supprimer").onclick=()=>{
            if (position=='portefeuille') {
                let nbTitres=lineNode.querySelector(".nbTitres").value;
                let valeurTitre=lineNode.querySelector(".valeur").innerHTML;
                modifyUserFnb(fnbName,nbTitres,valeurTitre,position,'remove')
                pieChart.removeData(fnbName);
                tabPortefeuilleBody.removeChild(lineNode);
                listportefeuille.splice(listportefeuille.indexOf(fnbName),1)
            }
            else{
                linearChart.removeDataset(fnbName);
                radarChart.removeDataset(fnbName);
                tabSelectionBody.removeChild(lineNode);
                listselection.splice(listselection.indexOf(fnbName),1)
                modifyUserFnb(fnbName,0,0,position,'remove')
            }
            modifyTableButtons()
        };
        lineNode.querySelector(".ajoutAutreTableau").onclick=function(){
            CreateModalFnb(fnbName,fnbData.titre, fnbData.close,fnbData.cluster);
        }
    }



    if (modalFnb) {
        document.body.removeChild(modalFnb);
        modalFnb=null;
    }

    modifyTableButtons()
}

function updateFnbDisplay(fnbName, lineNode,nbTitres, newNbTitres, valeur){ //update du portefeuille si on augmente le nombre de titres détenus

    let inputNbTitres=lineNode.querySelector(".nbTitres");
    lineNode.querySelector(".total").innerHTML=valeur*newNbTitres;

    pieChart.modifyData(fnbName,valeur*newNbTitres);

    modifyUserFnb(fnbName,parseInt(newNbTitres)-nbTitres,valeur,'portefeuille','update');
        
    inputNbTitres.onchange=()=>{
        if (inputNbTitres.value=='' || inputNbTitres.value<=0) { //on traite le changement de nb de titres seulement si la valeur est supérieure à 0 ou non nulle
            inputNbTitres.value=nbTitres;
        }
        else{
            updateFnbDisplay(fnbName, lineNode, nbTitres, inputNbTitres.value, valeur);
        }
    };
}


function modifyTableButtons(){
    let tabLines=tabPortefeuilleBody.querySelectorAll('.linePortefeuille');

    tabLines.forEach((line)=>{
        if (listselection.includes(line.querySelector(".symbole").innerHTML)) {
            line.querySelector(".ajoutAutreTableau").style.display='none';
        }
        else{
            line.querySelector(".ajoutAutreTableau").style.display='block';
        }
    });

    tabLines=tabSelectionBody.querySelectorAll('.lineSelection');

    tabLines.forEach((line)=>{
        if (listportefeuille.includes(line.querySelector(".symbole").innerHTML)) {
            line.querySelector(".ajoutAutreTableau").style.display='none';
        }
        else{
            line.querySelector(".ajoutAutreTableau").style.display='block';
        }
    });
}


function showCanvas(canvasNode){
    if (canvasNode!=activeChartNode) {
        canvasNode.style.display="block";
        activeChartNode.style.display="none";
        activeChartNode=canvasNode;
    }
}

function showTab(tabNode){
    if (tabNode!=activeTabNode) {
        tabNode.style.display="block";
        activeTabNode.style.display="none";
        activeTabNode=tabNode;
    }
}

//https://github.com/chartjs/Chart.js/issues/2292
//function get la value sur un clic dans la chart
function clickChart(chart, evt){
    let activePoints = chart.getElementsAtEvent(evt);
    let firstPoint = activePoints[0];
    let label = chart.data.labels[firstPoint._index];

    CreateModalFnb(label);

}

function getColor(seed, alpha, cluster){
    return randomColor({seed:seed,alpha: alpha,hue: clustersHues[cluster], hue: clustersHues[cluster],format: 'rgba'});
}
