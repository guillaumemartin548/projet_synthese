
<?php

require 'vendor/autoload.php'; // include Composer's autoloader

    class Connection{
        private static $connection = null;

		public static function getConnection() {
			if (Connection::$connection == null) {
				Connection::$connection = new MongoDB\Client("mongodb://localhost:27017");
			}

			return Connection::$connection;
		}

    }
