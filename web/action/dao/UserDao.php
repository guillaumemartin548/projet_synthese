<?php
    require_once("action/dao/Connection.php");
    require_once("action/dao/FnbDao.php");

	class UserDAO {

        //dictionnaire pour caller la bonne fonction avec un seul call Ajax
        private static $intents = [
            "addUser" => "UserDAO::addUser",
            "getUser"=> "UserDAO::getUser",
            "modifyFnb"=> "UserDAO::modifyFnb",
            "getFnbList"=> "UserDAO::getFnbList",
            "updateCash"=> "UserDAO::updateCash",
        ];

        public static function functionDispatch($data){
            //choix de la fonction à utiliser en fonction du intent envoyé
            $intent=$data['intent'];
            unset($data['intent']);
            return UserDAO::$intents[$intent]($data);

        }

        //ressource consultée pour savoir comment insérer un document dans mongoDB https://www.tutorialspoint.com/mongodb/mongodb_php.htm
        private static function addUser($data){//($data){
            $connection = Connection::getConnection();
            $collection = $connection->AppFNB->USERS;

            $document=$collection->insertOne($data);
            $_SESSION['email']=$data['email'];
            $_SESSION['password']=$data['password'];
            $_SESSION["visibility"]=1;

            return $document;

        }

        private static function getUser($data){ //a voir si retourne null s'il y a une erreur de password ou inexistant
            $connection = Connection::getConnection();

            $collection = $connection->AppFNB->USERS;

            $user=$collection->findOne(['email'=>$data['email']]);

            if (password_verify ( $data['password'], $user['password'])) {
                $_SESSION['email']=$data['email'];
                $_SESSION['password']=$data['password'];
                $_SESSION["visibility"]=1;
                return $user;
            }
            else{
                return false;
            }
        }


        private static function modifyUserData($data){
            $connection = Connection::getConnection();
            $collection = $connection->AppFNB->USERS;
        }

        private static function modifyFnb($data){  //id, array(fnb, portefeuille, selection, nb de titres)
            static $operations=[
                "add" => "UserDAO::addFnb",
                "remove"=> "UserDAO::removeFnb",
                "update"=> "UserDAO::updateFnb"
            ];

            $op=$data['operation'];
            unset($data['operation']);	
            return  $operations[$op]($data);
        }


        private static function addFnb($data){
            $connection = Connection::getConnection();
            $collection = $connection->AppFNB->USERS;
            $query=['$push'=>[$data['position']=>$data['symbole']]];

            if ($data['position']=='portefeuille') {
                $nbTitres=intval($data['nbTitres']);
                $prix=floatval($data['prix']);
                $montantTotal=$nbTitres*$prix;
                $query=['$push'=>[$data['position']=>['symbole'=>$data['symbole'],'nbTitres'=>[$nbTitres],'prix'=>[$prix],'date'=>[date("Y-m-d")]]],
                        '$inc'=>['fondsDisponibles'=>-$montantTotal]];
            }

            $updateResult=$collection->updateOne(
                ['email'=>$_SESSION['email']], 
                $query
            );
            return  printf("Matched %d document(s)\n", $updateResult->getMatchedCount());
        }

        private static function removeFnb($data){
            $connection = Connection::getConnection();
            $collection = $connection->AppFNB->USERS;
            $nbTitres=intval($data['nbTitres']);
            $prix=floatval($data['prix']);
            $montantTotal=$nbTitres*$prix;
            $query=['$pull'=>[$data['position']=>$data['symbole']]];

            if ($data['position']=='portefeuille') {
                $query=['$pull'=>[$data['position']=>['symbole'=>$data['symbole']]],
                        '$inc'=>['fondsDisponibles'=>$montantTotal]];
            }

            $updateResult=$collection->updateOne(
                ['email'=>$_SESSION['email']], 
                $query
            );
            return  $updateResult->getMatchedCount();
        }

        private static function updateFnb($data){ //update seulement 'a faire au portefeuille donc 'portefeuille.$.nbTitres' fonctionne
            $connection = Connection::getConnection();
            $collection = $connection->AppFNB->USERS;
            $nbTitres=intval($data['nbTitres']);
            $prix=floatval($data['prix']);
            $montantTotal=$nbTitres*$prix;


            $updateResult=$collection->updateOne(
                ['email'=>$_SESSION['email'], $data['position']=> ['$elemMatch'=> ["symbole"=> $data['symbole']]]]
                ,
                ['$push'=>[ 'portefeuille.$.nbTitres'=>$nbTitres, 'portefeuille.$.prix'=>$prix, 'portefeuille.$.date'=>date("Y-m-d")],
                 '$inc'=>['fondsDisponibles'=>-$montantTotal]]
            );
            
            return  $updateResult->getMatchedCount();
        }

        private  static function updateCash($data){
            $connection = Connection::getConnection();
            $collection = $connection->AppFNB->USERS;
            $floatAmount=floatval($data['fondsInvestis']);

            $updateResult=$collection->updateOne(
                ['email'=>$_SESSION['email']]
                ,['$inc'=>[ 'fondsInvestis'=>$floatAmount, 'fondsDisponibles'=>$floatAmount]]
            );
            
            return  $updateResult->getMatchedCount();
        }

        public static function getFnbList($UserId){

        }
    }
