<?php
    require_once("action/dao/Connection.php");

	class FnbDAO {

        public static function getOneFNB($symbole){ 
            $connection = Connection::getConnection();
            $collection = $connection->AppFNB->FNB;

            return $collection->findOne(['symbole'=>$symbole]);
        }


        public static function getFNBSearchBar(){ 
            $connection = Connection::getConnection();
            $collection = $connection->AppFNB->FNB;
            $result=[];

            $query=$collection->find([], ['projection' => ['bourse' => 1, 'symbole' => 1,'titre' => 1,'close' => 1, 'cluster' => 1]]);

            foreach ($query as $fnb) {
                array_push($result, $fnb);
            };

            return $result;
        }


        public static function getFnbPrice($symbole){ 
            $connection = Connection::getConnection();
            $collection = $connection->AppFNB->FNB;

            $result=$collection->findOne(['symbole'=>$symbole], ['close']);

            return $result;
        }

    }
