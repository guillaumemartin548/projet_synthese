<?php
    require_once("action/CommonAction.php");
    require_once("action/dao/FnbDao.php");
    require_once("action/dao/UserDao.php");


	class AjaxUserAction extends CommonAction {

		public $result;
		public $data;

		public function __construct() {
			parent::__construct(CommonAction::$VISIBILITY_PUBLIC);
		}

		protected function executeAction() {


			
			switch ($_POST['intent']) {
				case "addUser":
					$data=[
						'email'=>$_POST['email'],
						'password'=>password_hash($_POST["password"], PASSWORD_BCRYPT),
						'prenom'=>$_POST['prenom'],
						'nom'=>$_POST['nom'],
						'adresse'=>$_POST['adresse'],
						'dateNaissance'=>$_POST['dateNaissance'],
						'portefeuille'=>[],
						'selection'=>[],
						'fondsDisponibles'=>floatval($_POST['fondsDisponibles']),
						'fondsInvestis'=>floatval($_POST['fondsInvestis']),
						'typeClient'=>$_POST['typeClient'],
						'intent'=>$_POST['intent']
					];
					break;

				case "getUser":
					if ($_POST['email']==='alreadySignedIn') {
						$email =$_SESSION['email'];
						$password=$_SESSION['password'];
					}
					else {
						$email =$_POST['email'];
						$password=$_POST['password'];
					}
					$data=[
						'email'=>$email,
						'password'=>$password,
						'intent'=>$_POST['intent']
					];
					break;
				
				case "modifyFnb":
					$data=[
						'symbole'=>$_POST['symbole'],
						'nbTitres'=>$_POST['nbTitres'],
						'position'=>$_POST['position'],
						'intent'=>$_POST['intent'],
						'prix'=>$_POST['prix'],
						'operation'=>$_POST['operation'], //add remove update
					];
					break;		

				case "updateCash":
					$data=[
						'fondsInvestis'=>$_POST['fondsInvestis'],
						'intent'=>$_POST['intent']
					];
				
			}

			

				$this->result = UserDao::functionDispatch($data);

		}

		
	}
