<?php
	require_once("action/CommonAction.php");
    require_once("action/dao/FnbDao.php");
    require_once("action/dao/UserDao.php");


	class AjaxFnbAction extends CommonAction {

		public $result;

		public function __construct() {
			parent::__construct(CommonAction::$VISIBILITY_MEMBER);
		}

		protected function executeAction() {

			switch ($_POST['intent']) {
				case "getOneFNB":
					$this->result = FnbDAO::getOneFNB($_POST['symbole']);
					break;

				case "getFNBSearchBar":
					$this->result = FnbDAO::getFNBSearchBar();
					break;
			}
		}
	}