<?php
	require_once("action/CommonAction.php");

	class LogoutAction extends CommonAction {

		public function __construct() {
			parent::__construct(CommonAction::$VISIBILITY_MEMBER);
		}

		protected function executeAction() {
			session_unset();
			session_destroy();

			header("location:index.php");
			exit;
		}
	}
