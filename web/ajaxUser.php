
<?php
	require_once("action/ajax/AjaxUserAction.php");

	$action = new AjaxUserAction();
	$action->execute();

	echo json_encode($action->result);
