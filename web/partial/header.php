<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<meta http-equiv="X-UA-Compatible" content="ie=edge">
	<link href="https://fonts.googleapis.com/css?family=Lato&display=swap" rel="stylesheet">
	<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
	<script src="js/jquery.min.js"></script>
	<script src="https://cdnjs.cloudflare.com/ajax/libs/randomcolor/0.5.4/randomColor.min.js" integrity="sha256-vCJSGCdP2sT9Jc9MZTjK2oXAvE5bvDZ+hcFOqh0X/fc=" crossorigin="anonymous"></script>
	<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js" integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous"></script>
	<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>
	<script src="js/Chart.bundle.min.js"></script>
	<script src="js/mathFunctions.js"></script>
	<script src="js/LinearChart.js"></script>
	<script src="js/PieChart.js"></script>
	<script src="js/ScatterChart.js"></script>
	<script src="js/RadarChart.js"></script>
	<script src="js/main.js"></script>
	<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.8.0/Chart.min.css" integrity="sha256-aa0xaJgmK/X74WM224KMQeNQC2xYKwlAt08oZqjeF0E=" crossorigin="anonymous" />
	<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.3.1/css/all.css" integrity="sha384-mzrmE5qonljUremFsqc01SB46JvROS7bZs3IO2EmfFsd15uHvIt+Y8vEf7N7fWAU" crossorigin="anonymous">
	<link rel="stylesheet" href="css/global.css">

	<title>gestionFnb</title>
</head>
<body>
	<nav class ="navbar navbar-expand-lg navbar-light bg-info">
		<h2 class="navbar-brand text-light" href="index.php">GestionFnb</h2> 
		<button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNav" aria-controls="navbarNav" aria-expanded="false" aria-label="Toggle navigation">

    		<span class="navbar-toggler-icon"></span>
  		</button>

		<div class="collapse navbar-collapse" id="navbarNav">
			<ul class="navbar-nav ">
				<li class="nav-item ">
				<!-- vérifier les links à faire -->
					<button class="btn btn-outline-* text-light" type="button" id="buttonPortefeuille">Portefeuille</button>
				</li>
				<li class="nav-item dropdown">
				<!-- vérifier les links à faire -->
					<a class="nav-link dropdown-toggle text-light" href="#" id="navbarDropdownMenuLink" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
          				Comparateurs
       				 </a>
					
					<div class="dropdown-menu" aria-labelledby="navbarDropdownMenuLink">
          				<button class="dropdown-item" id="buttonLinear">Linéaire</button>
          				<button class="dropdown-item" id="buttonRadar">radar</button>
          				<button class="dropdown-item"id="buttonScatter">nuage de points</button>
        			</div>
				</li>
				<li class="nav-item">
				<!-- vérifier les links à faire -->
					<a href="" class="nav-link text-light">Paramètres du compte</a> 
				</li>




				<li class='mx-5'>
    				<input class="form-control mr-sm-2" type="search" placeholder="Recherche" aria-label="Search" id="searchBar" list="searchResults">
					<datalist id="searchResults">
						<!-- <option></option> -->
					</datalist>
				</li>
				<li class="nav-item mx-5">
					<form class="form-inline" action="logout.php" method="post">
    					<button class="btn btn-outline-info btn-light" type="submit">Deconnexion</button>
  					</form>
				</li>
			</ul>

		</div>

	</nav>
