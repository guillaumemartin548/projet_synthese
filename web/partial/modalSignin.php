<!-- Référence: https://mdbootstrap.com/docs/jquery/modals/forms/ -->
<!--  Référence:  https://bootsnipp.com/snippets/z8b1X-->
<form action="" method="post">
<div class="modal show" id="modalSignin" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true"
  data-backdrop="static" data-keyboard="false">
  <div class="modal-dialog cascading-modal" role="document">
    <!--Content-->
    <div class="modal-content">

      <!--Modal cascading tabs-->
      <div class="modal-c-tabs">

        <!-- Nav tabs -->
        <ul class="nav nav-tabs md-tabs tabs-2 light-blue darken-3" role="tablist">
          <li class="nav-item">
            <a class="nav-link active" data-toggle="tab" href="#panel7" role="tab"><i class="fas fa-user mr-1"></i>
              Connexion</a>
          </li>
          <li class="nav-item">
            <a class="nav-link" data-toggle="tab" href="#panel8" role="tab"><i class="fas fa-user-plus mr-1"></i>
              Enregistrement</a>
          </li>
        </ul>

        <!-- Tab panels -->
        <div class="tab-content">
          <!--Panel 7-->
          <div class="tab-pane fade in show active" id="panel7" role="tabpanel">

            <!--Body-->
            <div class="modal-body mb-1">

              <div class="form-group input-group">
                <div class="input-group-prepend">
                  <span class="input-group-text"> <i class="fa fa-envelope"></i> </span>
                </div>
                <input name="" class="form-control" placeholder="Courriel" type="email" id="emailConnection" required>
              </div> <!-- form-group// -->

              <div class="form-group input-group">
                <div class="input-group-prepend">
                  <span class="input-group-text"> <i class="fa fa-lock"></i> </span>
                </div>
                <input class="form-control" placeholder="Mot de passe" type="password" id="passwordConnection" required> 
              </div> <!-- form-group// -->

              <div class="text-center mt-2">
                <button class="btn btn-info" id="buttonConnection">Connexion</button>
              </div>
            </div>
            <!--Footer-->

          </div>
          <!--/.Panel 7-->

          <!--Panel 8-->
          <div class="tab-pane fade" id="panel8" role="tabpanel">

            <!--Body-->
            <div class="modal-body">
              <div class="form-group input-group">
                <div class="input-group-prepend">
                  <span class="input-group-text"> <i class="fa fa-user"></i> </span>
                </div>
                <input name="" class="form-control" placeholder="Prénom" type="text" id="firstNameSignup" required>
              </div> <!-- form-group// -->
              <div class="form-group input-group">
                <div class="input-group-prepend">
                  <span class="input-group-text"> <i class="fa fa-user"></i> </span>
                </div>
                <input name="" class="form-control" placeholder="Nom" type="text" id="lastNameSignup" required>
              </div> <!-- form-group// -->
              <div class="form-group input-group">
                <div class="input-group-prepend">
                  <span class="input-group-text"> <i class="far fa-calendar-alt"></i> </span>
                  <span class="input-group-text">date de naissance</span>
                </div>
                <div>
                  <input type="date" class="form-control" id="dateSignup" required>
                </div>
              </div>
              <div class="form-group input-group">
                <div class="input-group-prepend">
                  <span class="input-group-text"> <i class="fa fa-envelope"></i> </span>
                </div>
                <input name="" class="form-control" placeholder="Courriel" type="email" id="emailSignup" required>
              </div> <!-- form-group// -->
              <div class="form-group input-group">
                <div class="input-group-prepend">
                  <span class="input-group-text form-check-inline"> <i class="fas fa-venus-mars"></i> </span>
                </div>
                <div class="form-check form-check-inline">
                  <input class="form-check-input" type="radio" name="radioGender" id="maleRadio" value="M" required> 
                  <label class="form-check-label" for="inlineRadio1">Homme</label>
                </div>
                <div class="form-check form-check-inline">
                  <input class="form-check-input" type="radio" name="radioGender" id="femaleRadio" value="F" required>
                  <label class="form-check-label" for="inlineRadio2">Femme</label>
                </div>
                <div class="form-check form-check-inline">
                  <input class="form-check-input" type="radio" name="radioGender" id="otherRadio" value="X" required>
                  <label class="form-check-label" for="inlineRadio3">Autre</label>
                </div><!-- form-group// -->
              </div>

              <div class="form-group input-group">
                <div class="input-group-prepend">
                  <span class="input-group-text"> <i class="fas fa-map-marked-alt"></i></span>
                </div>
                <input type="text" class="form-control" id="addressSignup" placeholder="1234 rue Ontario" required>
              </div> <!-- form-group// -->

              <div class="form-group input-group">
                <div class="input-group-prepend">
                  <span class="input-group-text"> <i class="fa fa-building"></i> </span>
                </div>
                <select class="form-control" id="investorTypesSignup">
                  <option selected="">Votre type d'investisseur</option>
                  <option>Revenu</option>
                  <option>Équilibré</option>
                  <option>Croissance</option>
                </select>
              </div> <!-- form-group end.// -->

              <div class="form-group input-group">
                <div class="input-group-prepend">
                  <span class="input-group-text"> <i class="fas fa-dollar-sign"></i> </span>
                </div>
                <input class="form-control" placeholder="investissement initial $CAD" type="number" id="InitialAmountSignup" required>
              </div> <!-- form-group// -->

              <div class="form-group input-group">
                <div class="input-group-prepend">
                  <span class="input-group-text"> <i class="fa fa-lock"></i> </span>
                </div>
                <input class="form-control" placeholder="Mot de passe" type="password" id="passwordSignup" required>
              </div> <!-- form-group// -->
              <div class="form-group input-group">
                <div class="input-group-prepend">
                  <span class="input-group-text"> <i class="fa fa-lock"></i> </span>
                </div>
                <input class="form-control" placeholder="Confirmez le mot de passe" type="password" id="confirmPasswordSignup" required>
              </div> <!-- form-group// -->
              <div class="text-center form-sm mt-2">
                <button class="btn btn-info" id="buttonSignup">Enregistrement<i
                    class="fas fa-sign-in ml-1"></i></button>
              </div>
            </div>
          </div>
          <!--/.Panel 8-->
        </div>
      </div>
    </div>
    <!--/.Content-->
  </div>
</div>
</form>
<!--Modal: Login / Register Form-->