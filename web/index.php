<?php
	require_once("action/IndexAction.php");

	$action = new IndexAction();
	$action->execute();

	require_once("partial/header.php");
	require_once("partial/modalSelectFnb.php");
	require_once("partial/templateTable.php");
?>



<div id="divRendement">
		<div class='titleRendement'>
			<h4>Rendement:</h4>
			<h4 id="rendementTotal"></h4>
		</div>

		<div class='titleRendement'>
			<h4>Valeur totale:</h4>
			<h4 id="valeurTotale"></h4>
		</div>

		<div class="text-center">
			<div class="btn-group btn-light" id='buttonsTime'>
				<button type="button" class="btn" value=2500>10 ans</button>
				<button type="button" class="btn" value=1250>5 ans</button>
				<button type="button" class="btn" value=250>1 an</button>
				<button type="button" class="btn" value=125>6 mois</button>
				<button type="button" class="btn" value=60> 3 mois</button>
				<button type="button" class="btn" value=20>1 mois</button>
			</div>
		</div>
	</div>

<div class="contenu">
	<!-- à vérifier pour le canevas -->

	<div id="graphique">
		<div id="ChartsExScattPanel">
			<canvas id="PieChart" width="500" height="500"></canvas>
			<canvas id="LineChart" width="500" height="500"></canvas>
			<canvas id="RadarChart" width="500" height="500"></canvas>
			<div id="scatterChartPanel" >
			<div id="scatterChartRow1" class="d-flex align-items-center">
				<div class="dropdown">
					<button class="btn dropdown-toggle" type="button" id="scatterAxisY" data-toggle="dropdown">
					Dropdown button
					</button>
						<div class="dropdown-menu">
							<button class="btn dropdown-item" type="button">Test</button>
						</div>
				</div>
				<canvas id="ScatterChart" width="500" height="500"></canvas>
			</div>
			<div class="dropup text-center">
				<button class="btn dropdown-toggle" type="button" id="scatterAxisX" data-toggle="dropdown">
					Dropdown button
				</button>
					<div class="dropdown-menu">
						<button class="btn dropdown-item" type="button">Test</button>
					</div>
			</div>
		</div>
			
		</div>

		
	</div>

	<div id="tableau">
		<table class="table table-hover" id="tabPortefeuille">
			<thead class='table-secondary'>
				<tr>
					<th scope="col">Sélection</th>
					<th scope="col">Symbole</th>
					<th scope="col">Description</th>
					<th scope="col">Titres détenus</th>
					<th scope="col">Coût moyen</th>
					<th scope="col">Valeur</th>
					<th scope="col">Total</th>
					<th scope="col">Rendement</th>
					<!-- <th scope="col">Documentation</th> -->
					<!-- <th scope="col">Historique/Transactions</th> -->
					<th scope="col">Suppression</th>
				</tr>
			</thead>
			<tbody id="tabPorteSelection">
				<tr>
					<td></td>
					<td>Espèces</td>
					<td></td>
					<td></td>
					<td></td>
					<td></td>
					<td>
						<input type="number" min="0"step="10" id='fondsDispoPort'/>
					</td>
					<td></td>
					<td></td>
					<td></td>
				</tr>
			</tbody>
		</table>

		<table class="table table-hover" id="tabSelection">
			<thead class='table-secondary'>
				<tr>
				<th scope="col">Ajout/Portefeuille</th>
				<th scope="col">Symbole</th>
				<th scope="col">Description</th>
				<th scope="col">Valeur</th>
				<!-- <th scope="col">Documentation</th> -->
				<th scope="col">Suppression</th>
				</tr>
			</thead>
			<tbody id="tabBodySelection">
			</tbody>
		</table>


	</div>

</div>

<?php
	if (!$action->isLoggedIn()) {  //évite de repartir le modal signin si le user à garder sa session active ou un cookie, A FAIRE
		require_once("partial/modalSignin.php");
		?>
		<script>
			$('#modalSignin').modal('show')
		</script>
<?php
	}
	require_once("partial/footer.php");