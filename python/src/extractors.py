import sys
import urllib.request
import csv
import xlrd
from bs4 import BeautifulSoup, SoupStrainer

class Extractor:
    def __init__(self):
        self.file=None
        self.workfile=None
        self.workfileName=None
        self.url=None
    
    def extract(self, URL, filename, directory, fileExtension):
        self.url=URL
        self.file=urllib.request.urlopen(self.url).read()
        self.workfileName=directory+filename+fileExtension
        self.workfile=open(self.workfileName, "wb")
        self.workfile.write(self.file)
        self.workfile.close()

class ExcelExtractor(Extractor):
    def __init__(self):
        Extractor.__init__(self)
        self.excelFile=None
        
    def extract(self, URL, filename, directory, fileExtension, columns, sheet, startLine):
        #à replacer à la fin
        Extractor.extract(self,URL, filename, directory, fileExtension)
        #enlever le self.workfile
        #self.workfileName=directory+filename+fileExtension
        return self.parseFile(columns, sheet, startLine)
        
    
    def parseFile(self, columns, sheet, startLine):
        excelWorkbook=xlrd.open_workbook(self.workfileName)
        excelSheet=excelWorkbook.sheet_by_index(sheet)
        data=[]
        newEntry=None
        
        while startLine < excelSheet.nrows:
            
            newEntry=[]
            for col in columns:
                newEntry.append(excelSheet.cell_value(startLine, col))
            
            data.append(newEntry)
            startLine+=1
  
        return data   
    

class HtmlExtractor(Extractor):
    def __init__(self):
        Extractor.__init__(self)
        
    def extract(self, URL, filename, directory, fileExtension,tags):
        Extractor.extract(self,URL, filename, directory, fileExtension)
        return self.parseFile(tags)
    
    def parseFile(self, tags):
        searchedPart= SoupStrainer(class_="col-xl-2dot4 col-lg-3 col-md-4 col-6")
        htmldoc=BeautifulSoup(open(self.workfileName),"html.parser", parse_only=searchedPart)
        data=[]        
        while htmldoc.strong:
            data.append(htmldoc.strong.get_text())
            htmldoc.strong.extract()
        return data
            
    
class CsvExtractor(Extractor):
    def __init__(self):
        Extractor.__init__(self)
        self.csvFile=None
        
    def extract(self, URL, filename, directory,fileExtension):
        Extractor.extract(self,URL, filename, directory, fileExtension)
        return self.parseFile()
        
    
    def parseFile(self):
        data=[]
        with open(self.workfileName) as csvFile:
            reader=csv.reader(csvFile)  
            for row in reader:
                data.append(row)                    
        return data
    

def main():
    return 0
 
    
if __name__=='__main__':
    sys.exit(main())
    