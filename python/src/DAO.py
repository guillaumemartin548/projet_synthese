from pymongo import MongoClient
from fnb import Fnb
from datetime import datetime
import sys

class Connection:
    client = None

    def verifyConnection(self):
        if Connection.client is None:
            try:
                Connection.client = MongoClient('localhost', 27017)
                db = Connection.client.database_names()
            except Exception as e:
                Connection.client = None

        return Connection.client is not None

    def closeConnection(self):
        Connection.client.close()


class FnbDAO:
    
    def addFNB(self,fnb):  
        document={
        "bourse": fnb.bourse,
        "symbole": fnb.symbole,
        "titre": fnb.titre,
        "description": fnb.description,
        "inceptionDate": fnb.inceptionDate,
        "emetteur":fnb.emetteur,
        "UrlEmetteur":fnb.Urlemetteur,#information non dispo
        "fraisGestion":fnb.fraisGestion,
        "sharesNB": fnb.sharesNB,
        "marketCap":fnb.marketCap,
        "HistoriqueValeur":fnb.HistoriqueValeur,
        "open":fnb.open,
        "close": fnb.close,
        "dayRange":fnb.dayRange,
        "movingAverages":fnb.movingAverages,
        "volume":fnb.volume,
        "averageVolume":fnb.averageVolume,
        "dividend":fnb.dividend,
        "PEratio":fnb.PEratio,
        "PBratio":fnb.PBratio,
        "eps":fnb.eps,
        "cluster":fnb.cluster
        }
        
        Connection.client.AppFNB.FNB.insert(document)
    
    def updateFNB(self,fnb):
        
        #self.updateFNBValueHistory(fnb)
        
        document={
        "sharesNB": fnb.sharesNB,
        "marketCap":fnb.marketCap,
        "HistoriqueValeur":fnb.HistoriqueValeur,
        "open":fnb.open,
        "close": fnb.close,
        "dayRange":fnb.dayRange,
        "movingAverages":fnb.movingAverages,
        "volume":fnb.volume,
        "averageVolume":fnb.averageVolume,
        "dividend":fnb.dividend,
        "PEratio":fnb.PEratio,
        "PBratio":fnb.PBratio,
        "eps":fnb.eps,
        "cluster":fnb.cluster
        }
        Connection.client.AppFNB.FNB.update_one({"symbole": fnb.symbole},{"$set":document})
    
    def initializeFnbCollection(self, listFnb):
        for fnb in listFnb:
            self.addFNB(fnb)
    
    def updateFnbCollection(self, listFnb):
        for fnb in listFnb:
            self.updateFNB(fnb)
    
    def isEmpty(self):
        Connection.verifyConnection(self)
        return Connection.client.AppFNB.FNB.count()
    
    def run(self, listFnb):
        if self.isEmpty():
            self.updateFnbCollection(listFnb)
        else:
            self.initializeFnbCollection(listFnb)
        
    #def cappedArrayField(self):
        #return 0
    
    #def updateFNBValueHistory(self,fnb):
        #date=datetime.now()
        #dateFormat=date.strftime("%m/%d/%Y")
        #print(fnb.symbole)
        #filterQuery={"symbole": fnb.symbole}
        #query={'$push':{"HistoriqueValeur.date": 'test'}}
        #query={'$push':{"HistoriqueValeur.date": 'test',"HistoriqueValeur.valeurOuverture": 'test',"HistoriqueValeur.valeurFermeture": 'test',"HistoriqueValeur.valeurMin": 'test',"HistoriqueValeur.valeurMax": 'test'}}
        #Connection.client.AppFNB.FNB.update_one(filterQuery,query)
def main():
    return 0
 
    
if __name__=='__main__':
    sys.exit(main())
        