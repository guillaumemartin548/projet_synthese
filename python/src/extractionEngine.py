import sys
import os
from datetime import datetime
from extractors import ExcelExtractor
from extractors import HtmlExtractor
from extractors import CsvExtractor
from DAO import FnbDAO
from fnb import Fnb
import re
import financialFunction
import random

class ExtractionEngine:
    def __init__(self,urlExcel, urlHtml,  urlCsv, columnsExcel, tagsHtml,collections,pathData):
        self.urlExcel=urlExcel
        self.columnsExcel=columnsExcel
        self.urlHtml=urlHtml
        self.tagsHtml=tagsHtml
        self.urlCsv=urlCsv
        self.collections=collections
        self.listFNB=[]
        self.excelExtractor=ExcelExtractor()
        self.htmlExtractor=HtmlExtractor()
        self.csvExtractor=CsvExtractor()
        self.FnbDAO= FnbDAO()
        self.path=os.path.join(pathData, '')
    
    def run(self):
        self.buildDataBase()
        #if self.FnbDAO.isEmpty():
         #   self.updateDataBase()
        #else:
         #   self.buildDataBase()
    
    def buildDataBase(self):
        self.structureData()
        self.FnbDAO.run(self.listFNB)
    
    def updateDataBase(self):
        self.structureExcelData()
        self.structureHTMLData()
        self.FnbDAO.run(self.listFNB)
    
    def structureData(self):
        self.structureExcelData()
        self.structureCSVData()
        self.structureHTMLData()
    
    def structureExcelData(self):
        excelData=self.excelExtractor.extract(self.urlExcel, 'TSXdata',self.path, '.xlsx', self.columnsExcel,1, 7)
        for data in excelData:
            newFnb=Fnb()
            newFnb.bourse=data[0]
            newFnb.titre=data[1]
            newFnb.symbole=data[2]
            newFnb.emetteur=data[3]
            newFnb.inceptionDate=data[4]
            self.listFNB.append(newFnb)
            
    def structureCSVData(self):
        csvData=[]
        
        #à vérifier, peutêtre fonction séparée
        enddate=datetime.now()
        startdate=datetime(enddate.year-10,enddate.month,enddate.day)
        
        strEndDate=enddate.strftime("%m/%d/%Y")
        strStartDate=startdate.strftime("%m/%d/%Y")
        
        for fnb in self.listFNB:
            #faire le bon lien url avec le format
            url=self.urlCsv.format(fnb.symbole,strStartDate,strEndDate)
            csvData=self.csvExtractor.extract(url, fnb.symbole, self.path,'.csv')
            #pour enlever la ligne de titre des fichiers csv
            csvData.pop(0)
            for csvLine in csvData:
                date=csvLine[0]
                fnb.HistoriqueValeur['dates'].append(date)
                fnb.HistoriqueValeur['valeurOuverture'].append(float(csvLine[1]))
                fnb.HistoriqueValeur['valeurMax'].append(float(csvLine[2]))
                fnb.HistoriqueValeur['valeurMin'].append(float(csvLine[3]))
                fnb.HistoriqueValeur['valeurFermeture'].append(float(csvLine[4]))
                fnb.HistoriqueValeur['volume'].append(float(csvLine[5]))
            
            fnb.movingAverages['50day']=financialFunction.average(fnb.HistoriqueValeur['valeurFermeture'], 50)
            fnb.movingAverages['100day']=financialFunction.average(fnb.HistoriqueValeur['valeurFermeture'], 100)
            fnb.movingAverages['200day']=financialFunction.average(fnb.HistoriqueValeur['valeurFermeture'], 200)
            fnb.averageVolume=financialFunction.average(fnb.HistoriqueValeur['volume'], 30)  
        
    ##étant donné que les données ne sont pas toujours complète sur le site web, je dois ajouter des try except pour chaque cast de valeur et mettre la valeur à 0 si l'exception est lancée   
    ##L'autre option étant de ne pas ajouté le FNB a la base de données malgré qu'il pourrait y avoir une bonne quantité d'informations valides quand même
    def structureHTMLData(self): 
        for fnb in self.listFNB:
            url=self.urlHtml.format(fnb.symbole)
            htmlData=self.htmlExtractor.extract(url, fnb.symbole, self.path, '.html', self.tagsHtml)
            try:
                fnb.open=float(htmlData[0])
            except:
                fnb.open=0
            try:
                fnb.dayRange['maximum']=float(htmlData[1])
            except:
                fnb.dayRange['maximum']=0
            try:
                fnb.sharesNB=int(re.sub(",", "", htmlData[3]))
            except:
                fnb.sharesNB=0
            
            try:
                fnb.close=float(htmlData[5])
            except:
                fnb.close=0
            
            try:
                fnb.dayRange['minimum']=float(htmlData[6])
            except:
                fnb.dayRange['minimum']=0
            try:
                fnb.marketCap=float(re.sub(",", "", htmlData[8]))
            except:
                fnb.marketCap=0
            try:
                fnb.PEratio=float(htmlData[12])
            except:
                fnb.PEratio=0
            try:
                fnb.eps=float(htmlData[13])
            except:
                fnb.eps=0
            try:
                fnb.dividend=float(htmlData[14])
            except:
                fnb.dividend=0
            try:
                fnb.PBratio=float(htmlData[16])
            except:
                fnb.PBratio=0
            fnb.cluster=random.randrange(0,5)  #Temporaire si je n'ai pas le temps d'inclure le clustering

def main():

    #excel(fichier avec les infos de bases pour identifier les fnb)
    #'https://www.tsx.com/resource/en/1168'

    #csv
    #"https://quotes.wsj.com/etf/CA/XTSE/ZUQ/historical-prices/download?MOD_VIEW=page&num_rows=90&range_days=90&startDate=06/17/2019&endDate=09/15/2019"
    
    #html
    #'https://web.tmxmoney.com/quote.php?qm_symbol=ZUQ'
    
    urlExcel='https://www.tsx.com/resource/en/1168'
    columnsExcel=[1,2,3,7,9]
    urlHtml='https://web.tmxmoney.com/quote.php?qm_symbol={}'
    tagsHtml=['strong']
    urlCsv='https://quotes.wsj.com/etf/CA/XTSE/{0}/historical-prices/download?MOD_VIEW=page&num_rows=4000&range_days=4000&startDate={1}&endDate={2}'
    collections=['FNB']
    
    extraction=ExtractionEngine(urlExcel,urlHtml,  urlCsv, columnsExcel, tagsHtml, collections, '../data/')
    extraction.run()
      
    return 0
    
if __name__=='__main__':
    sys.exit(main())
    