import sys

class Fnb:
    def __init__(self):
        self.bourse=None
        self.symbole=None
        self.titre=None
        self.description=None
        self.inceptionDate=None
        self.emetteur=None
        self.Urlemetteur=None
        self.fraisGestion=None
        self.sharesNB=None
        self.marketCap=None
        self.HistoriqueValeur={'dates':[], 'valeurOuverture':[], 'valeurFermeture':[], 'valeurMin':[], 'valeurMax':[],  'volume':[]}
        self.open=None
        self.close=None
        self.dayRange={'minimum':0, 'maximum':0}
        self.movingAverages={'50day':0, '100day':0, '200day':0}
        self.volume=None
        self.averageVolume=None  #investopedia, calculé habituellement aux 20-30 jours
        self.dividend=None
        self.PEratio=None
        self.PBratio=None
        self.eps=None
        self.cluster=None
        
        
def main():
    return 0
 
    
if __name__=='__main__':
    sys.exit(main())
        
